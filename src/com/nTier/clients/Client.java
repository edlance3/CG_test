package com.nTier.clients;
import java.util.ArrayList;
import java.util.List;

import com.nTier.events.rentals.Guest;
import com.nTier.events.rentals.HotelRoom;
import com.nTier.events.rentals.RentalCar;
import com.nTier.events.rentals.RentalManager;
import com.nTier.events.reports.Report;
import com.nTier.events.reservation.CarReservation;
import com.nTier.events.reservation.HotelReservation;
import com.nTier.events.reservation.Insurable;
import com.nTier.events.reservation.Reservation;
import com.nTier.events.reservation.ReservationException;

/*
 * comment added for Git Lab 04.1...and updated on git lab site 9/3 at 1035am
 */
class Client {
	
	public static void main(String[] args) {
		RentalCar car1 = new RentalCar(44);
		RentalCar car2 = new RentalCar(22);
		HotelRoom room1 = new HotelRoom(11);
		HotelRoom room2 = new HotelRoom(5);
		
		car1.setId(1);
		car1.setMake("VW");;
		car1.setModel("Rabbit");
		car2.setId(3);
		car2.setMake("Honda");;
		car2.setModel("Accord");
		
		room1.setRoomNumber(55);
		room1.setSmoking(true);
		room2.setRoomNumber(77);
		room2.setSmoking(false);
		
		System.out.println("car1 id: " + car1.getId() + " - Make:" + car1.getMake()
				+ " - Model:" + car1.getModel());
		System.out.println("car2 id: " + car2.getId() + " - Make:" + car2.getMake()
				+ " - Model:" + car2.getModel());
		
		System.out.println("room1: " + "roomnumber=" + room1.getRoomNumber()
				+ " smoking preference=" + room1.isSmoking());
		System.out.println("room2: " + "roomnumber=" + room2.getRoomNumber()
				+ " smoking preference=" + room2.isSmoking());
		
		System.out.println(" ------------------------------");
		RentalManager manager = new RentalManager();
		//manager.initialize();
//		System.out.println(manager.showAvailableCars());
//		System.out.println(manager.showAvailableRooms());
		manager.showAvailableCars();
		manager.showAvailableRooms();
		
		//for lab 7.1
		Guest guest = new Guest("joe", "Brown", "N9872225252", "N776666662255", "872662613636363636363636");
		CarReservation carRez = new CarReservation(guest, car1);
		HotelReservation hotelRez = new HotelReservation(guest, room1, true);
		System.out.println("--------");
		System.out.println(carRez.validateDriverLicenseNumber());
		System.out.println(carRez.validateRentalCarClubNumber());
		System.out.println(hotelRez.validateHotelClubNumber());
		
		boolean reserved = carRez.reserve();
		System.out.println("car reserved=" + reserved);
		//System.out.println("room reserved=" + hotelRez.reserve());
		
		//lab 7.4
		//Reservation carRes = carRez;
	
		
		//lab 7.5
		manager.showAvailableCars();
		manager.showAvailableRooms();
		
		RentalCar car3 = new RentalCar(7);
		RentalCar car4 = new RentalCar(7);
		RentalCar car5 = new RentalCar(8);
		System.out.println(car3.equals(car4));
		System.out.println(car3.equals(car5));
		
		HotelRoom room3 = new HotelRoom(87);
		HotelRoom room4 = new HotelRoom(87);
		HotelRoom room5 = new HotelRoom(109);
		System.out.println(room3.equals(room4));
		System.out.println(room3.equals(room5));
		
		
		CarReservation r;
	    r = new CarReservation();
		r.setGuest(guest);
		
		System.out.println("---------------------------");
		
		// of course it's an instance of CarReservation
				System.out.println((r instanceof CarReservation));

				// a CarReservation IS-A Reservation
				System.out.println((r instanceof Reservation));

				// a CarReservation IS-A Object
				System.out.println((r instanceof Object));

				// a CarReservation IS-A Insurable
				System.out.println((r instanceof Insurable));
				
				Reservation rez = r;
				boolean b = ((CarReservation)rez).insure(10.0);
				System.out.println("downcast: " + b);
				
				Insurable ins = r;
     			boolean bb = ins.insure(99);
    			System.out.println("using Insurable ref: " + bb);
    			
    			//lab 10.1
    			System.out.println ("Lab 10.1 ------------------------------");
    			manager.showAvailableCars();
    			manager.showAvailableRooms();
    			manager.showAvailableSeats();
    			
    			Guest newGuest1 = new Guest("joe", "zeble", "N9872225252", "N776666662255", "872662613636363636363636");
    			Guest newGuest2 = new Guest("louie", "hello", "N987222252525252", "N774747474255", "872999999111113636363636");
    			Guest newGuest3 = new Guest("heather", "ableson", "N98725263652525252", "N711111114255", "8729995555555563636");
    			Guest newGuest4 = new Guest("ttt", "teller", "N9872552525252", "N774747474255", "872999999111113636363636");
    			
    			List<Guest> guestList = new ArrayList<Guest>();
    			guestList.add(newGuest1);
    			guestList.add(newGuest2);
    			guestList.add(newGuest3);
    			guestList.add(newGuest4);
    			Report report = new Report();
    			System.out.println (report.displayGuestReport(guestList));
    			
    			
    			//lab 11.2
    			try {
					System.out.println("testing reserve method for exception");
    				hotelRez.getRoom().setRoomNumber(20000);
					hotelRez.reserve();
				} catch (ReservationException e) {
					System.out.println("bad reservation # - exception thrown");
					System.out.println("...but program continues onward normally");
				}

	} // end main

}

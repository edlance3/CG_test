package com.nTier.events.reservation;

import com.nTier.events.rentals.Guest;
import com.nTier.events.rentals.HotelRoom;
import com.vendor.RoomsRUs;

public class HotelReservation extends Reservation {
	private HotelRoom room;
	private boolean shuttleNeeded;

	public HotelReservation() {
	}

	public HotelReservation(Guest guest, HotelRoom room) {
		super(guest);
		this.setRoom(room);
	}

	public HotelReservation(Guest guest, HotelRoom room, boolean shuttleNeeded) {
		this(guest, room);
		this.setShuttleNeeded(shuttleNeeded);
	}
	
	/*
	 * Validates guest's hotel club number.
	 */
	public boolean validateHotelClubNumber() {
		// call inherited validation method, passing in guest.hotelClubNumber
		return this.validateClubNumber(this.getGuest().getHotelClubNumber());
	}

	@Override
	public boolean reserve() throws ReservationException {
		boolean result = false;
		RoomsRUs rus = new RoomsRUs();
		String theResponse = new String( rus.reserve(getRoom().getRoomNumber()) );
		result = "valid".equals(theResponse.substring(3, 8));
		
		if (!result){
			throw new ReservationException();
		}
		
		return result;
		
		/* Optional - use of RegEx:
		 *   replace line 38 with:
		 * 		 result = theResponse.matches(".{3}valid.*");  
		 */
	}
	
	public HotelRoom getRoom() {
		return room;
	}

	public void setRoom(HotelRoom room) {
		this.room = room;
	}

	public boolean isShuttleNeeded() {
		return shuttleNeeded;
	}

	public void setShuttleNeeded(boolean shuttleNeeded) {
		this.shuttleNeeded = shuttleNeeded;
	}
	
	
	
	
	
}

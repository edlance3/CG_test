package com.nTier.events.reservation;

public interface Insurable {
	
	public boolean insure(double payment);
	
}

package com.nTier.events.reservation;

public class CreditCard {
	private String cardNumber;
	private String secCode;
	private String expDate;
	private String fName;
	private String lName;

	public CreditCard() {
	}

	public CreditCard(String cardNumber, String secCode, String expDate,
			String fName, String lName) {
		this.setCardNumber(cardNumber);
		this.setSecurityCode(secCode);
		this.setExpDate(expDate);
		this.setFirstName(fName);
		this.setLastName(lName);
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSecurityCode() {
		return secCode;
	}

	public void setSecurityCode(String secCode) {
		this.secCode = secCode;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getFirstName() {
		return fName;
	}

	public void setFirstName(String name) {
		fName = name;
	}

	public String getLastName() {
		return lName;
	}

	public void setLastName(String name) {
		lName = name;
	}
	
	
}

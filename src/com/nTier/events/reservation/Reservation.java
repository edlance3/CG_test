package com.nTier.events.reservation;

import com.nTier.events.rentals.Guest;

public class Reservation {
	private Guest guest;
	
	public Reservation() {	
	}

	public Reservation(Guest guest) {
		this.guest = guest;
	}

	/*
	 * All club numbers are valid if starts with 'N' and is at least 8
	 * characters long.
	 */
	boolean validateClubNumber(String clubNumber) {
		return (clubNumber != null && clubNumber.charAt(0) == 'N' 
				&& clubNumber.length() >= 8);
	}
	
	//default reserve() behavior unless overridden
	public boolean reserve() throws ReservationException {
		return false;
	}
	
	public boolean holdDeposit(CreditCard card) {
		boolean answer = false;
		String secCode = card.getSecurityCode();
		answer = verifySecurityCode(secCode);
		return answer;
	}

		/* Alternate solution using RegEx:
		 * 		String regex = "\\d{3}";  //any digit, exactly 3 times
				if (secCode.matches(regex)) {
					answer = true;
				}
		 */
	
	public boolean holdDeposit(double depositAmount) {
		return depositAmount >= 50;
	}
	
	private boolean verifySecurityCode(String secCode) {
		boolean rightLength = secCode.length() == 3;
		boolean allDigits = true;
		for (int i = 0; i < secCode.length(); i++) {
			//inspect each Character value in the String and 
			// check whether it is a Digit or not:
			if (! Character.isDigit(secCode.charAt(i))) {
				allDigits = false;
			}
		}
		return (rightLength && allDigits);
	}
	
	
	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	
}

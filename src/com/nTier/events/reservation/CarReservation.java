package com.nTier.events.reservation;

import com.nTier.events.rentals.Guest;
import com.nTier.events.rentals.RentalCar;
import com.ntier.event.util.EmailSender;

public class CarReservation extends Reservation implements Insurable {
	private RentalCar car;

	public CarReservation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CarReservation(Guest guest, RentalCar car) {
		super(guest);
		this.setCar(car);
	}

	/*
	 * Validates guest's rental car club number.
	 */
	public boolean validateRentalCarClubNumber() {
		// call inherited validation method, passing in
		// guest.rentalCarClubNumber
		return this.validateClubNumber(this.getGuest().getRentalCarClubNumber());
	}

	/*
	 * Driver license numbers are valid if >= 12 in length.
	 */
	public boolean validateDriverLicenseNumber() {
		String driverLicenseNumber = this.getGuest().getDriversLicenseNumber();
		return driverLicenseNumber != null
				&& driverLicenseNumber.length() >= 12;
	}

	@Override
	public boolean reserve() {
		boolean reserved = false;
		//local boolean values to pass into 'send' method:
		boolean validDL = validateDriverLicenseNumber();
		boolean validRCCnumber = validateRentalCarClubNumber();
		//3rd party functionality:
		EmailSender sender = new EmailSender();
		String answer = sender.send(validDL, validRCCnumber);
		reserved = ("true".equals(answer));
		return reserved;
	}
	
	public RentalCar getCar() {
		return car;
	}

	public void setCar(RentalCar car) {
		this.car = car;
	}

	@Override
	public boolean insure(double payment) {
		return (payment == 10.0);
	}
	
	
	
}

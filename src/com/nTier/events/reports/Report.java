package com.nTier.events.reports;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.nTier.events.rentals.Guest;
import com.nTier.events.rentals.Rentable;


public class Report {
	
	public static final String FOOTER = "Copyright Event Management Gurus 2009";
	private static final Object GUEST_HEADER = "GUEST REPORT:";

	//Lab11.1 update = Overloaded signature to 'displayInventory'
	public void displayInventory(Rentable car) {
		System.out.println(car.toString());
		
	}
	
    // lab 10.1 - Collections
	public String displayGuestReport(List<Guest> guests) {
		
		System.out.println(" \nBefore sort: --------------------------------");
		for (Guest g: guests) {
			System.out.println("=====");
			System.out.println(g);
		}
		
		Collections.sort(guests);
		System.out.println(" \nAfter sort: ------------------------------------");
		for (Guest g: guests) {
			System.out.println("=====");
			System.out.println(g);
		}
		
		
		StringBuilder guestReport = new StringBuilder(500);
		guestReport.append("\n").append(GUEST_HEADER).append("\n");
		guestReport.append("Report Date: ").append(new Date()).append("\n\n");
		for (Guest guest : guests) {
			guestReport.append(guest).append("\n").append("****************").append("\n");
		}

		guestReport.append("\n").append(FOOTER).append("\n");

		return guestReport.toString();
	}
	
	
}

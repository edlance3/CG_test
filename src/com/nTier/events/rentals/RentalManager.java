package com.nTier.events.rentals;
import java.util.HashSet;
import java.util.Set;

import com.nTier.events.reports.Report;


public class RentalManager {
	// available cars and hotel rooms.  Static as of Lab 6.5
//	private static RentalCar[] availableCars;
//	private static HotelRoom[] availableRooms;
//	private static PlaneSeat[] availableSeats;
	
	// updated from Array to Set (no duplicates) in Lab 10.1
	private static Set<Rentable> availableCars;
	private static Set<Rentable> availableRooms;
	private static Set<Rentable> availableSeats;
	private Report report = new Report();
	
	public RentalManager() {
		RentalManager.initialize();
	}
	
	//can't make this static because it uses "report," which is an instance variable
	 public void showAvailableCars(){
		

		 // lab 10.1
		 for (Rentable r: getAvailableCars()){
			 report.displayInventory(r); 
		 }
		 
		
	}
	
	 public void showAvailableRooms(){
		
		// lab 10.1
				 for (Rentable r: getAvailableRooms()){
					 report.displayInventory(r); 
				 }
		//return allRooms;
	}
	 
	 public void showAvailableSeats(){
			
			// lab 10.1
					 for (Rentable r: getAvailableSeats()){
						 report.displayInventory(r); 
					 }
			//return allRooms;
		}
	
	private static void initialize(){
		//instantiate the arrays
//		availableCars = new RentalCar[5];
//		availableRooms = new HotelRoom[4];
		
		availableCars = new HashSet<Rentable>();
		availableRooms = new HashSet<Rentable>();
		
		RentalCar car = new RentalCar(11);
		car.setMake("Toyota");
		car.setModel("Matrix");
		availableCars.add(car);

		car = new RentalCar(22);
		car.setMake("Honda");
		car.setModel("Civic");
		car.setYear(1985);
		availableCars.add(car);

		car = new RentalCar(33);
		car.setMake("Nissan");
		car.setModel("Quest");
		car.setYear(1994);
		availableCars.add(car);

		car = new RentalCar(44);
		car.setMake("Ford");
		car.setModel("Explorer");
		car.setYear(2004);
		availableCars.add(car);

		car = new RentalCar(55);
		car.setMake("Mazda");
		car.setModel("Mazda3");
		availableCars.add(car);
		
		// create the rooms and add them to the array
		HotelRoom room = new HotelRoom(16);
		room.setSmoking(true);
		room.setNumberOfBeds(2);
		availableRooms.add(room);

		room = new HotelRoom(12);
		room.setSmoking(false);
		availableRooms.add(room);

		room = new HotelRoom(4);
		room.setSmoking(true);
		room.setNumberOfBeds(2);
		availableRooms.add(room);

		room = new HotelRoom(42);
		room.setSmoking(false);
		availableRooms.add(room);
		
		availableSeats = new HashSet<Rentable>();

		// create the seats and add them to the array
		PlaneSeat seat = new PlaneSeat("1A", "SEA", "SAN");
		availableSeats.add(seat);
		seat = new PlaneSeat("2A", "BMI", "SEA");
		availableSeats.add(seat);
		seat = new PlaneSeat("3A", "JFK", "EYW");
		availableSeats.add(seat);

	}
	// no setters since they're initialized with the data we want for now.
		public Set<Rentable> getAvailableCars() {
			return availableCars;
		}

		public Set<Rentable> getAvailableRooms() {
			return availableRooms;
		}

		public Set<Rentable> getAvailableSeats() {
			return availableSeats;
		}
}
